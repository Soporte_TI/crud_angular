import { Component } from '@angular/core';

import { PuntosService } from './puntos.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // title = 'CRUDAngularMySQL';
  puntos = null;

  punto = {
    idPunto: null,
    nombre: null,
    nivel: null,
    fecha: null,
    coordenadas: null
  }

  constructor(private puntosServicio: PuntosService) { }

  ngOnInit() {
    this.obtenerPuntos();
  }

  obtenerPuntos() {
    this.puntosServicio.obtenerPuntos().subscribe(
      result => this.puntos = result
    );
  }

  altaPunto() {
    this.puntosServicio.altaPunto(this.punto).subscribe(
      datos => {
        if(datos['resultado'] == 'OK') {
          alert(datos['mensaje']);
          this.obtenerPuntos();
        }
      }
    );
  }

  bajaPunto(idPunto) {
    this.puntosServicio.bajaPunto(idPunto).subscribe(
      datos => {
        if(datos['resultado'] == 'OK') {
          alert(datos['mensaje']);
          this.obtenerPuntos();
        }
      }
    );
  }

  editarPunto() {
    this.puntosServicio.editarPunto(this.punto).subscribe(
      datos => {
        if(datos['resultado'] == 'OK') {
          alert(datos['mensaje']);
          this.obtenerPuntos();
        }
      }
    );
  }

  seleccionarPunto(idPunto) {
    this.puntosServicio.seleccionarPunto(idPunto).subscribe(
      result => this.punto = result[0]
    );
  }

  hayRegistros() {
    if(this.puntos == null) {
      return false;
    } else {
      return true;
    }
  }
}
