import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PuntosService {


  URL = "http://localhost/api/";
  constructor(private http: HttpClient) { }

  obtenerPuntos() {
    return this.http.get(`${this.URL}ObtenerPuntos.php`);
  }

  altaPunto(usuario) {
    return this.http.post(`${this.URL}AltaPunto.php`, JSON.stringify(usuario));
  }

  bajaPunto(idPunto: number) {
    return this.http.get(`${this.URL}BajaPunto.php?idPunto=${idPunto}`);
  }

  seleccionarPunto (idPunto: number) {
    return this.http.get(`${this.URL}SeleccionarPunto.php?idPunto=${idPunto}`)
  }

  editarPunto(usuario) {
    return this.http.post(`${this.URL}EditarPunto.php`, JSON.stringify(usuario))
  }
}
